package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;

    public static void main(String[] args) {
        ///// Oppgave 6
        // Opprett to Pokémon-objekter
        pokemon1 = new Pokemon("Charmander", 100, 20);
        pokemon2 = new Pokemon("Squirtle", 100, 30);

      
        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            
            pokemon1.attack(pokemon2);

            
            if (!pokemon2.isAlive()) {
                System.out.println(pokemon2.getName() + " is defeated by " + pokemon1.getName() + ".");
                break; 
            }

            
            pokemon2.attack(pokemon1);

            
            if (!pokemon1.isAlive()) {
                System.out.println(pokemon1.getName() + " is defeated by " + pokemon2.getName() + ".");
                break; 
            }
        }
    }
}